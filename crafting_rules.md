# Downtime Activities

There are a number of downtime activities available to you individually. These are a series of skill checks and a time-committment from your characters to see through the process. Some are more complicated than others, with a lot more preparation and more serious ramifications. Others are simply to read a book to gain knowledge, buying and selling of magical items, and so on.

-----------------------------------------------------

The Raven Queen is the deity of Life and Death